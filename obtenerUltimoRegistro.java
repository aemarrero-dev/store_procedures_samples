//Obtiene el registro de ese rut mas reciente

             public static SolicitudDAO callSPFicoUlt(String rut, String productCategory) {

                    ConexionSQL con = new ConexionSQL();

                    SolicitudDAO s=null;

                    if(con.conectar()) {

                          

                           CallableStatement cst;

                           try {

                                  cst = con.getConexion().prepareCall("{call dbo.sp_fico_ult(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");

                                  cst.setString(1, rut);

                            cst.setString(2, productCategory);

                            cst.registerOutParameter(3, Types.INTEGER);

                            cst.registerOutParameter(4, Types.VARCHAR);

                            cst.registerOutParameter(5, Types.VARCHAR); //DATE

                            cst.registerOutParameter(6, Types.INTEGER);

                            cst.registerOutParameter(7, Types.VARCHAR);

                            cst.registerOutParameter(8, Types.VARCHAR);

                            cst.registerOutParameter(9, Types.VARCHAR);

                            cst.registerOutParameter(10, Types.INTEGER);

                            cst.registerOutParameter(11, Types.INTEGER);

                            cst.registerOutParameter(12, Types.INTEGER);

                            cst.registerOutParameter(13, Types.INTEGER);

                            cst.registerOutParameter(14, Types.INTEGER);

                            cst.registerOutParameter(15, Types.VARCHAR);

                            cst.registerOutParameter(16, Types.VARCHAR);

                            cst.registerOutParameter(17, Types.INTEGER);

                            cst.execute();

                           

                            s = new SolicitudDAO();

                            s.setId(cst.getInt(3));

                           s.setRut(cst.getString(4));

                           s.setLogDate(cst.getString(5));

                           s.setDiffDate(cst.getInt(6));

                           s.setLogTime(cst.getString(7));

                           s.setCallType(cst.getString(8));

                           s.setProduct(cst.getString(9));

                           s.setFlagResubmissionNM(cst.getInt(10));

                           s.setFlagResubmissionM(cst.getInt(11));

                          

                           s.setVigencyNM(cst.getInt(12));

                           s.setVigencyM(cst.getInt(13));

                           s.setFicoID(cst.getInt(14));

                           s.setTypeSend(cst.getString(15));

                           s.setCrossReference(cst.getString(16));

                           s.setVigency(cst.getInt(17));

                          

                           /*

                            System.out.println("id: " + cst.getInt(3));

                            System.out.println("rut: " + cst.getString(4));

                            System.out.println("logdate: " + cst.getString(5));

                            System.out.println("diffdate: " + cst.getInt(6));

                            System.out.println("logtime: " + cst.getString(7));

                            System.out.println("calltype: " + cst.getString(8));

                            System.out.println("productCategory: " + cst.getString(9));

                            System.out.println("flagResubmissionNM: " + cst.getInt(10));

                            System.out.println("flagResubmisionM: " + cst.getInt(11));

                            System.out.println("vigencyNM: " + cst.getInt(12));

                            System.out.println("vigencyM: " + cst.getInt(13));

                            System.out.println("ficoId: " + cst.getInt(14));

                            System.out.println("typesend: " + cst.getString(15));

                            System.out.println("crossReference: " + cst.getString(16));

                            System.out.println("vigency: " + cst.getInt(17));

                            */

                           System.out.println(s.toString());

                           } catch (SQLException e) {

                                  e.printStackTrace();

                           } finally {

                                  con.desconectar();

                           }

                    }

                    return s;

             }

 

/*LLamada

 

SolicitudDAO sol = QueriesDB.callSPFicoUlt("12345", "Multi");*/