//Inserta solicitud en BD

             public static int callSPFicoUpdateVig(int id, int vigency) {

                    int result = 0;

                    ConexionSQL con = new ConexionSQL();

                    if(con.conectar()) {

                          

                           CallableStatement cst;

                           try {

                                  cst = con.getConexion().prepareCall("{call dbo.sp_fico_upd_vig(?, ?, ?)}");

                                  cst.setInt(1, id);

                            cst.setInt(2, vigency);

                            cst.registerOutParameter(3, Types.INTEGER);

                            cst.execute();

                            result = cst.getInt(3);

 

                           } catch (SQLException e) {

                                  e.printStackTrace();

                           } finally {

                                  con.desconectar();

                           }

                    }

                    return result;

             }

 

 

//Como llamarlo
//System.out.println("UPDATE: " + QueriesDB.callSPFicoUpdateVig(10033, 100));