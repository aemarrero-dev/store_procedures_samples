//Inserta solicitud en BD

             public static int callSPFicoInsert(SolicitudDAO s, int operation) {

                    int result = 0;

 

                    ConexionSQL con = new ConexionSQL();

                    if(con.conectar()) {

                          

                           CallableStatement cst;

                           try {

                                  cst = con.getConexion().prepareCall("{call dbo.sp_fico_ins(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");

                                  cst.setString(1, s.getRut());

                            cst.setString(2, s.getLogDate());

                            cst.setInt(3, s.getDiffDate());

                            cst.setString(4, s.getLogTime());

                            cst.setString(5, s.getCallType());

                            cst.setString(6, s.getProduct());

                            cst.setInt(7, s.getFlagResubmissionNM());

                            cst.setInt(8, s.getFlagResubmissionM());

                            cst.setInt(9, s.getVigencyNM());

                            cst.setInt(10, s.getVigencyM());

                            cst.setInt(11, s.getFicoID());

                            cst.setString(12, s.getTypeSend());

                            cst.setString(13, s.getCrossReference());

                            cst.setInt(14, s.getVigency());

                            cst.setInt(15, operation);

                            cst.registerOutParameter(16, Types.INTEGER);

                            cst.execute();

                            result = cst.getInt(16);

 

                           } catch (SQLException e) {

                                  e.printStackTrace();

                           } finally {

                                  con.desconectar();

                           }

                    }

                    return result;

             }

 

/*Como llamarlo:

SolicitudDAO s = new SolicitudDAO();

             s.setRut("919196");

             s.setLogDate("2020-04-26");

             s.setDiffDate(0);

             s.setLogTime("13:37:57.0000000");

             s.setCallType("P1");

             s.setProduct("Multi");

             s.setFlagResubmissionNM(1);

             s.setFlagResubmissionM(1);

             s.setVigencyNM(3);

             s.setVigencyM(3);

             s.setFicoID(14);

             s.setTypeSend("Request");

             s.setCrossReference("1232");

             s.setVigency(4);

             Parámetro operation = 0 (RUT NUEVO)

System.out.println("REGISTRO NUEVO: " + QueriesDB.callSPFicoInsert(s, 0));

Parámetro operation = 1 (RUT EXISTENTE)

             System.out.println("REGISTRO YA EXISTENTE: " + QueriesDB.callSPFicoInsert(s, 1));*/