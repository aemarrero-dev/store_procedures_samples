/***********************************************************************
   Procedimiento : vrf_pgo_spg
   Archivo       :
   Tablas        : bd_pri_36.admi.tb_det_pgo_spg
                   bd_pri_36.admi.tb_atr_usu
                   bd_pri_36.admi.tb_cnv

   Parametros    : c_cnl      = Codigo de Canal
                   n_rut_cnv  = Rut del Cliente (Convenio)
                   n_rut_usu  = Rut del Usuario
                   c_prc      = Codigo del Producto
                   n_prc      = Numero del Producto
                   c_trsspg   = Codigo TransacciÃ³n Servipag
                   monto      = Monto a cancelar
                   c_trs      = Codigo de la Transaccion

   Retorno       : 0      - OK
 		   9008   - Transaccion Fuera de Horario Habil
                   9116   - Pagos estan cancelados
                   9117   - Cuenta Corriente en Dolares

   Funcion       : Verificar transaccion de PAGO SERVICIO SERVIPAG
   Modificacion  : RFV20020405 Se agrega nueva verificacion de monto maximo
                   para las transferencias
               IBS20030526 Se modifica logica por cambios en codigos de producto
                           debido a migracion en DataPro
************************************************************************/
/*
       Plog/Re : 20201
       Fecha : 22 de Agosto de 2011
       Autor : PRH
       Accion : - Correccion de observaciones periodo garantia. 
	   
	   Plog/Re : TEC-255
       Fecha : MAyo de 2019
       Autor : EO
       Accion : Se incorporan filtros para fraudes de pago por boton de pago servipag / tabla de parametros con codigos de comercios a filtrar. 

*/
CREATE PROC admi.vrf_pgo_spg
( @c_cnl         char(03), 
  @nrut_cnv      char(09),
  @nrut_usu      char(09),
  @c_prc         char(05),
  @n_prc         char(20),
  @c_trsspg      char(30),
  @monto         char(13),
  @montomaxpgo   char(13),
  @c_trs         char(06),
  @n_reg         char(180), /* Plog 20201 */
  @tipdsp        char(2)
)
AS

DECLARE @status  int, @c_tip_cnl char(03)
DECLARE @fechoy  char(8), @horanow char(4), @flag    char(1)
DECLARE @mtotrf  money
DECLARE @mtomax  money
DECLARE @mtomaxdia  money
DECLARE @mtomaxtrs  money
DECLARE @fecacu    char(8)
DECLARE @cprctabla    char(5)
DECLARE @totcalc   money   /* Plog 20201 */
DECLARE @i         int     /* Plog 20201 */
DECLARE @reg       char(1) /* Plog 20201 */

DECLARE @cspgemppgo  char(5)
   
  
  IF EXISTS (select 1 from admi.tb_cnv
	    where c_tip_cnl = @c_cnl
              and n_rut_num_cli = @nrut_cnv
              and c_tip_cnv = 'N')
    SELECT @nrut_usu = n_rut_num_usu
      FROM bd_pri_36.admi.tb_det_pgo_spg
     WHERE c_tip_cnl     = @c_cnl
       AND n_rut_num_cli = @nrut_cnv
       AND c_trs_spg     = @c_trsspg   


	
 SELECT DISTINCT @cspgemppgo=c_spg_emp_pgo
      FROM bd_pri_36.admi.tb_det_pgo_spg
     WHERE c_tip_cnl     = @c_cnl
       AND n_rut_num_cli = @nrut_cnv
       AND c_trs_spg     = @c_trsspg 
	   and c_spg_doc_pgo != '0' 

select @mtomaxdia=v_montmaxdia,@mtomaxtrs=v_montmax from bd_pri_36.admi.tb_param_spg
where c_key like @cspgemppgo+@tipdsp


  SELECT @fechoy = CONVERT( char(8), GETDATE(), 112 )
  SELECT @mtotrf  = CONVERT(MONEY,@monto)
  SELECT @totcalc = 0 /* Plog 20201 */

  IF @c_trs != ' '
  BEGIN

     EXEC @status = bd_pri_36.admi.vrf_trs_usu_inf @c_cnl, @nrut_cnv, @nrut_usu,
                                                @c_prc, @n_prc, @c_trs

     IF @status != 0
       RETURN @status

     /* Valido el horario de la transaccion y que sea un dia habil */

     SELECT @horanow = SUBSTRING( CONVERT( char(5), GETDATE(), 8 ), 1, 2 ) +
        	       SUBSTRING( CONVERT( char(5), GETDATE(), 8 ), 4, 2 )


/*   IBS20030526

     EXEC bd_ahorros.admi.fec_des_hab @fechoy , @flag output

     IF @flag = 'N' 
	RETURN 9008

     IF (@horanow < '0800') OR (@horanow > '1400')
      	RETURN 9008
*/    

     /* Obtengo datos de montos restrictivos del producto-convenio */
     SELECT /*@mtomaxdia = v_max_dia,
            @mtomaxtrs = v_max_trs,*/
            @mtomax = v_acu_dia,
            @cprctabla = c_prc,
            @fecacu = CONVERT( char(8), f_acu_dia, 112 )
       FROM bd_pri_36.admi.tb_prc_cnv
      WHERE n_rut_num_cli = @nrut_cnv
        AND c_tip_cnl = @c_cnl
        AND SUBSTRING(c_prc,1,2) = SUBSTRING(@c_prc,1,2)
        AND n_prc = @n_prc
        AND c_edo_prc = '1'

     IF (@cprctabla = '01004' OR @cprctabla = '01008') /* SCM007 */
       RETURN 9117

   /*  IF (@mtomaxdia = null OR @mtomaxtrs = null)
       RETURN 9010 */

     IF (@fechoy != @fecacu)
       SELECT @mtomax = 0

     IF (@mtomaxdia != -1 )
     BEGIN
       IF (@mtotrf + @mtomax > @mtomaxdia) 
          RETURN 9047
     END

     IF (@mtomaxtrs != -1 )
     BEGIN
       IF (@mtotrf > @mtomaxtrs)
          RETURN 9048
     END

  END
  ELSE
  BEGIN
     /* Obtengo datos de montos restrictivos del producto-convenio */
     SELECT @mtomax = isnull(v_acu_otr_bco_lib,0) ,
            @fecacu = CONVERT( char(8), f_acu_dia, 112 )
       FROM bd_pri_36.admi.tb_prc_cnv
      WHERE n_rut_num_cli = @nrut_cnv
        AND c_tip_cnl = @c_cnl
        AND SUBSTRING(c_prc,1,2) = SUBSTRING(@c_prc,1,2)
        AND n_prc = @n_prc
        AND c_edo_prc = '1'


     IF @@rowcount = 0
       RETURN 9000

     IF (@fechoy != @fecacu)
       SELECT @mtomax = 0

     /*SELECT @mtomaxtrs  = CONVERT(MONEY,@montomaxpgo)*/

	
     IF (@mtomaxdia != -1 )
     BEGIN
       IF (@mtotrf + @mtomax > @mtomaxdia)
          RETURN 9093
     END
     
      IF (@mtomaxtrs != -1 )
     BEGIN
       IF (@mtotrf > @mtomaxtrs)
          RETURN 9209
     END
  END

  SELECT @c_tip_cnl=c_tip_cnl
  FROM bd_pri_36.admi.tb_det_pgo_spg
  WHERE c_tip_cnl     = @c_cnl     and
        n_rut_num_cli = @nrut_cnv  and
        n_rut_num_usu = @nrut_usu  and
        c_trs_spg     = @c_trsspg  and
        t_acz         = 'C'

  IF @@rowcount <> 0
     RETURN 9116

  /* Verificar total pagado */
  SELECT @i = 1                                  /* Plog 20201 */
  WHILE (@i <= 180)                              /* Plog 20201 */
  BEGIN                                          /* Plog 20201 */
    SELECT @reg = substring(@n_reg, @i, 1)       /* Plog 20201 */
    IF (@reg = '1')                              /* Plog 20201 */
    BEGIN                                        /* Plog 20201 */
      SELECT @totcalc = @totcalc + v_det_pgo     /* Plog 20201 */
      FROM bd_pri_36.admi.tb_det_pgo_spg         /* Plog 20201 */
      WHERE c_tip_cnl     = @c_cnl     and       /* Plog 20201 */
            n_rut_num_cli = @nrut_cnv  and       /* Plog 20201 */
            n_rut_num_usu = @nrut_usu  and       /* Plog 20201 */
            c_trs_spg     = @c_trsspg  and       /* Plog 20201 */
            n_det_pgo     = CONVERT(int, @i)     /* Plog 20201 */
    END                                          /* Plog 20201 */
    IF (@reg = 'F') BREAK                        /* Plog 20201 */
    SELECT @i = @i + 1                           /* Plog 20201 */
  END                                            /* Plog 20201 */
 

  IF (@mtotrf != @totcalc )                      /* Plog 20201 */
    RETURN 777                                   /* Plog 20201 */

  RETURN 0