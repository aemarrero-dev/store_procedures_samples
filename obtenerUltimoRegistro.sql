USE [HUB]

GO

/****** Object:  StoredProcedure [dbo].[sp_fico_ult]    Script Date: 11-11-2019 17:47:13 ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

ALTER PROCEDURE [dbo].[sp_fico_ult]  

    @rut varchar(50),  

       @productCategory varchar(50),

       @id_o int OUTPUT,

       @rut_o varchar(50) OUTPUT,

       @logDate_o date OUTPUT,

       @diffDate_o int OUTPUT,

       @logTime_o varchar(50) OUTPUT,

       @callType_o varchar OUTPUT,

       @productCatg_o varchar OUTPUT,

       @flagResubNM_o int OUTPUT,

       @flagResubM_o int OUTPUT,

       @vigencyNM_o int OUTPUT,

       @vigencyM_o int OUTPUT,

       @ficoId_o int OUTPUT,

       @typeSend_o varchar OUTPUT,

       @crossRef_o varchar OUTPUT,

       @vigency_o int OUTPUT         

AS

BEGIN

       SET NOCOUNT ON;

       --consulto ultimo registro segun rut y producto 

    SELECT

       @id_o=id, @rut_o=rut, @logDate_o=logDate, @diffDate_o=diffDate,

       @logTime_o=logTime, @callType_o=callType, @productCatg_o=productCategory,

       @flagResubNM_o=flagResubmissionNM, @flagResubM_o=flagResubmissionM, @vigencyNM_o=vigencyNM, @vigencyM_o=vigencyM,

       @ficoId_o=ficoId, @typeSend_o=typeSend, @crossRef_o=crossReference, @vigency_o=vigency

    FROM dbo.Solicitud 

    WHERE rut = @rut AND productCategory = @productCategory

       ORDER BY id DESC

--RETURN

--GO

END