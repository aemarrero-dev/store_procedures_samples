USE [HUB]

GO

/****** Object:  StoredProcedure [dbo].[sp_fico_ins]    Script Date: 11-11-2019 14:40:43 ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

ALTER PROCEDURE [dbo].[sp_fico_ins]  

    @rut varchar(50),  

    @logDate date,

       @diffDate int,

       @logTime time(7),

       @callType varchar(50),

       @productCategory varchar(50),

       @flagResubmissionNM int,

       @flagResubmissionM int,

       @vigencyNM int,

       @vigencyM int,

       @ficoId int,

       @typeSend varchar(50),

       @crossReference varchar(50),

       @vigency int,

       @operation int,

       @result_o int OUTPUT

                

AS   

    DECLARE @idFico int

      

       --REGISTRO NUEVO

       IF @operation = 0

       BEGIN

             SELECT @idFico = ISNULL(MAX(ficoId), 0) FROM dbo.Solicitud

            

             SET NOCOUNT ON;

             --consulto si existe registro con ese rut

             SELECT TOP 1 * 

             FROM dbo.Solicitud 

             WHERE rut = @rut

 

             -- SE VALIDA QUE SEA UN REGISTRO NUEVO

             IF @@ROWCOUNT = 0

             BEGIN

            

                    SELECT @idFico = @idFico + 1

                    INSERT INTO dbo.Solicitud (rut, logDate, diffDate, logTime, callType, productCategory, flagResubmissionNM, flagResubmissionM, vigencyNM, vigencyM, ficoId, typeSend, crossReference, vigency)

                    VALUES (@rut, @logDate, @diffDate, @logTime, @callType, @productCategory, @flagResubmissionNM, @flagResubmissionM, @vigencyNM, @vigencyM, @idFico, @typeSend, @crossReference, @vigency)

                    SELECT @result_o = 0

             END

             ELSE SELECT @result_o = 1

       END

       ELSE --REGISTRO YA EXISTENTE

       BEGIN

             INSERT INTO dbo.Solicitud (rut, logDate, diffDate, logTime, callType, productCategory, flagResubmissionNM, flagResubmissionM, vigencyNM, vigencyM, ficoId, typeSend, crossReference, vigency)

             VALUES (@rut, @logDate, @diffDate, @logTime, @callType, @productCategory, @flagResubmissionNM, @flagResubmissionM, @vigencyNM, @vigencyM, @ficoId, @typeSend, @crossReference, @vigency)

             SELECT @result_o = 0

       END

       RETURN @result_o