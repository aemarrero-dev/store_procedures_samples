USE [HUB]

GO

 

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

ALTER PROCEDURE [dbo].[sp_fico_upd_vig]  

    @id bigint,  

       @vigency int,

       @result_o int OUTPUT

                

AS

BEGIN

       IF @id IS NULL

       BEGIN

             PRINT 'ERROR: Parameter id is Null' 

             SET @result_o = 1

       END

       ELSE

       BEGIN

             SELECT TOP 1 * 

             FROM dbo.Solicitud 

             WHERE id = @id

 

             -- SE VALIDA QUE EXISTA UN REGISTRO QUE ACTUALIZAR

             IF @@ROWCOUNT = 0

             BEGIN

                    SET @result_o = 1

             END

             ELSE

             BEGIN

                    SET NOCOUNT ON;

                    --se actualiza vigencia segun la solicitud por Id.

                    UPDATE dbo.Solicitud 

                    SET vigency = @vigency 

                    WHERE id = @id

 

                    IF @@ERROR <> 0

                    BEGIN

                           SET @result_o = @@ERROR

                    END

                    ELSE

                    BEGIN

                           SET @result_o = 0

                    END

             END

 

             /*SET NOCOUNT ON;

             --se actualiza vigencia segun la solicitud por Id.

             UPDATE dbo.Solicitud 

             SET vigency = @vigency 

             WHERE id = @id

 

             IF @@ERROR <> 0

             BEGIN

                    SET @result_o = @@ERROR

             END

             ELSE

             BEGIN

                    SET @result_o = 0

             END */

       END
END