declare @mes int

  declare @anio int

  declare @m int

  declare @y int

  set @m = DATEPART(month, getdate())

  select @m

  set @y = DATEPART(year, getdate())

  select @y

  IF  @m = 1

  BEGIN

         set @mes = 12

         set @anio = @y-1

         select @mes

         select @anio

  END

  ELSE

  BEGIN

         set @mes = @m-1

         select @mes

  END