USE bd_pri_36
GO

IF OBJECT_ID('admi.vrf_pgo_spg') IS NOT NULL
BEGIN
    DROP PROC admi.vrf_pgo_spg
    PRINT '<<< DROPPED PROC admi.vrf_pgo_spg >>>'
END
go

/***********************************************************************
   Procedimiento : vrf_pgo_spg
   Archivo       :
   Tablas        : bd_pri_36.admi.tb_det_pgo_spg
                   bd_pri_36.admi.tb_atr_usu
                   bd_pri_36.admi.tb_cnv

   Parametros    : c_cnl      = Codigo de Canal
                   n_rut_cnv  = Rut del Cliente (Convenio)
                   n_rut_usu  = Rut del Usuario
                   c_prc      = Codigo del Producto
                   n_prc      = Numero del Producto
                   c_trsspg   = Codigo TransacciÃ³n Servipag
                   monto      = Monto a cancelar
                   c_trs      = Codigo de la Transaccion

   Retorno       : 0      - OK
                                 9008   - Transaccion Fuera de Horario Habil
                   9116   - Pagos estan cancelados
                   9117   - Cuenta Corriente en Dolares

   Funcion       : Verificar transaccion de PAGO SERVICIO SERVIPAG
   Modificacion  : RFV20020405 Se agrega nueva verificacion de monto maximo
                   para las transferencias
               IBS20030526 Se modifica logica por cambios en codigos de producto
                           debido a migracion en DataPro
************************************************************************/
/*
       Plog/Re : 20201
       Fecha : 22 de Agosto de 2011
       Autor : PRH
       Accion : - Correccion de observaciones periodo garantia. 

*/
CREATE PROC admi.vrf_pgo_spg
( @c_cnl         char(03), 
  @nrut_cnv      char(09),
  @nrut_usu      char(09),
  @c_prc         char(05),
  @n_prc         char(20),
  @c_trsspg      char(30),
  @monto         char(13),
  @montomaxpgo   char(13),
  @c_trs         char(06),
  @n_reg         char(180), /* Plog 20201 */
  @tipdsp        char(2)
)
AS

DECLARE @status  int, @c_tip_cnl char(03)
DECLARE @fechoy  char(8), @horanow char(4), @flag    char(1)
DECLARE @mtotrf  money
DECLARE @mtomax  money
DECLARE @mtomaxdia  money
DECLARE @mtomaxtrs  money
DECLARE @fecacu    char(8)
DECLARE @cprctabla    char(5)
DECLARE @totcalc   money   /* Plog 20201 */
DECLARE @i         int     /* Plog 20201 */
DECLARE @reg       char(1) /* Plog 20201 */

DECLARE @cspgemppgo  char(5)

  DECLARE @c_cnl         char(03) 
  DECLARE @nrut_cnv      char(09)
  DECLARE @nrut_usu      char(09)
  DECLARE @c_prc         char(05)
  DECLARE @n_prc         char(20)
  DECLARE @c_trsspg      char(30)
  DECLARE @monto         char(13)
  DECLARE @montomaxpgo   char(13)
  DECLARE @c_trs         char(06)
  DECLARE @n_reg         char(180) /* Plog 20201 */
  DECLARE @tipdsp        char(2)
  DECLARE @detpago  money
  DECLARE @n         int

  /* PC, PT
select   @c_cnl       ='070'
select   @nrut_cnv    ='010100227' 
select   @nrut_usu    ='000030068'
select   @c_prc       ='0100' 
select   @n_prc       ='00000000000970194827'
select   @c_trsspg    ='00974127'
select   @monto       ='20000'
select   @montomaxpgo ='100000'
select   @c_trs       ='F00007'
select   @n_reg       ='3' 
select   @tipdsp      ='PC'  */


select   @c_cnl       ='070'
select   @nrut_cnv    ='007359034' 
select   @nrut_usu    ='000000001'
select   @c_prc       ='0300' 
select   @n_prc       ='00000000000970194827'
select   @c_trsspg    ='CLT0000120191125125193'
select   @monto       ='20000'
select   @montomaxpgo ='100000'
select   @c_trs       ='F00007'
select   @n_reg       ='' 
select   @tipdsp      ='PT' 

  /*
select   @c_cnl       ='070'
select   @nrut_cnv    ='005140065' 
select   @nrut_usu    ='013272329'
select   @c_prc       ='0100' 
select   @n_prc       ='00000000000970194827'
select   @c_trsspg    ='00974127'
select   @monto       ='20000'
select   @montomaxpgo ='100000'
select   @c_trs       ='F00007'
select   @n_reg       ='' 
select   @tipdsp      ='PT' */

  /*IF EXISTS (select 1 from bd_pri_36.admi.tb_cnv
                    where c_tip_cnl = @c_cnl
              and n_rut_num_cli = @nrut_cnv
              and c_tip_cnv = 'N') */
    
     /*OBTENGO RUT DE USUARIO, PARA PERSONAS ES EL MISMO, PARA EMPRESAS ES DISTINTO DE n_rut_num_cli */
     SELECT @nrut_usu = n_rut_num_usu
     FROM bd_pri_36.admi.tb_det_pgo_spg
     WHERE c_tip_cnl     = @c_cnl
     AND n_rut_num_cli = @nrut_cnv
     AND c_trs_spg     = @c_trsspg   
                
    /*OBTENGO CODIGO SPG */
	SELECT DISTINct @cspgemppgo=c_spg_emp_pgo
      FROM bd_pri_36.admi.tb_det_pgo_spg
     WHERE c_tip_cnl     = @c_cnl
       AND n_rut_num_cli = @nrut_cnv
       AND c_trs_spg     = @c_trsspg 
       and c_spg_doc_pgo != '0' 
       
    INSERT INTO bd_pri_36.admi.tb_temp_spg (cspgemppgo, mtomaxdia, mtomaxtrs, detpago, totcalc, descrip) VALUES (@cspgemppgo, 0, 0, 0, 0, 'codigo emp spg')

       
/*OBTENGO MONTO POR DIA Y MONTO POR TRANSACCION DESDE TABLA DE PARAMETROS SPG */
select @mtomaxdia=v_montmaxdia,@mtomaxtrs=v_montmax from bd_pri_36.admi.tb_param_spg
where c_key like @cspgemppgo+@tipdsp

INSERT INTO bd_pri_36.admi.tb_temp_spg (cspgemppgo, mtomaxdia, mtomaxtrs, detpago, totcalc, descrip) VALUES (@cspgemppgo, @mtomaxdia, @mtomaxtrs, 0, 0, 'montomaxdia y montomaxtrs')

  SELECT @fechoy = CONVERT( char(8), GETDATE(), 112 )
  SELECT @mtotrf  = CONVERT(MONEY,@monto)
  SELECT @totcalc = 0 /* Plog 20201 */

  IF @c_trs != ' '
  BEGIN

     EXEC @status = bd_pri_36.admi.vrf_trs_usu_inf @c_cnl, @nrut_cnv, @nrut_usu,
                                                @c_prc, @n_prc, @c_trs

     IF @status != 0
       SELECT @status 

     /* Valido el horario de la transaccion y que sea un dia habil */

     SELECT @horanow = SUBSTRING( CONVERT( char(5), GETDATE(), 8 ), 1, 2 ) +
                       SUBSTRING( CONVERT( char(5), GETDATE(), 8 ), 4, 2 )  

     /* Obtengo datos de montos restrictivos del producto-convenio */
     SELECT /*@mtomaxdia = v_max_dia,
            @mtomaxtrs = v_max_trs,*/
            @mtomax = v_acu_dia,
            @cprctabla = c_prc,
            @fecacu = CONVERT( char(8), f_acu_dia, 112 )
       FROM bd_pri_36.admi.tb_prc_cnv
      WHERE n_rut_num_cli = @nrut_cnv
        AND c_tip_cnl = @c_cnl
        AND SUBSTRING(c_prc,1,2) = SUBSTRING(@c_prc,1,2)
        AND n_prc = @n_prc
        AND c_edo_prc = '1'

     IF (@cprctabla = '01004' OR @cprctabla = '01008') /* SCM007 */
       SELECT 9117

   /*  IF (@mtomaxdia = null OR @mtomaxtrs = null)
       RETURN 9010 */

     IF (@fechoy != @fecacu)
       SELECT @mtomax = 0

     IF (@mtomaxdia != -1 )
     BEGIN
       IF (@mtotrf + @mtomax > @mtomaxdia) 
          SELECT 9047
     END

     IF (@mtomaxtrs != -1 )
     BEGIN
       IF (@mtotrf > @mtomaxtrs)
          SELECT 9048
     END

  END
  ELSE
  BEGIN
     /* Obtengo datos de montos restrictivos del producto-convenio */
     SELECT @mtomax = isnull(v_acu_otr_bco_lib,0) ,
            @fecacu = CONVERT( char(8), f_acu_dia, 112 )
       FROM bd_pri_36.admi.tb_prc_cnv
      WHERE n_rut_num_cli = @nrut_cnv
        AND c_tip_cnl = @c_cnl
        AND SUBSTRING(c_prc,1,2) = SUBSTRING(@c_prc,1,2)
        AND n_prc = @n_prc
        AND c_edo_prc = '1'


     IF @@rowcount = 0
       SELECT 9000

     IF (@fechoy != @fecacu)
       SELECT @mtomax = 0

     /*SELECT @mtomaxtrs  = CONVERT(MONEY,@montomaxpgo)*/

                
     IF (@mtomaxdia != -1 )
     BEGIN
       IF (@mtotrf + @mtomax > @mtomaxdia)
          SELECT 9093
     END
     
      IF (@mtomaxtrs != -1 )
     BEGIN
       IF (@mtotrf > @mtomaxtrs)
          SELECT 9209
     END
  END

  /**/
  SELECT @c_tip_cnl=c_tip_cnl
  FROM bd_pri_36.admi.tb_det_pgo_spg
  WHERE c_tip_cnl     = @c_cnl     and
        n_rut_num_cli = @nrut_cnv  and
        n_rut_num_usu = @nrut_usu  and
        c_trs_spg     = @c_trsspg  and
        t_acz         = 'C'

  IF @@rowcount <> 0
     SELECT 9116


select @n = COUNT(*) 
from bd_pri_36.admi.tb_det_pgo_spg
where c_tip_cnl = @c_cnl
and n_rut_num_cli = @nrut_cnv
and c_trs_spg = @c_trsspg

  /* Verificar total pagado */
  SELECT @i = 1                                  /* Plog 20201 */
  WHILE (@i <= @n)                              /* Plog 20201 */
  BEGIN                                          /* Plog 20201 */
    SELECT @reg = substring(@n_reg, @i, 1)       /* Plog 20201 */
      
	  /* SELECT @totcalc = @totcalc + v_det_pgo     
      FROM bd_pri_36.admi.tb_det_pgo_spg         
      WHERE c_tip_cnl     = @c_cnl     and       
            n_rut_num_cli = @nrut_cnv  and       
            n_rut_num_usu = @nrut_usu  and       
            c_trs_spg     = @c_trsspg  and       
            n_det_pgo     = CONVERT(int, @i) */
            
          SELECT @detpago = v_det_pgo, @cspgemppgo = c_spg_emp_pgo 
	      FROM bd_pri_36.admi.tb_det_pgo_spg         
	      WHERE c_tip_cnl     = '070'     and       
	            n_rut_num_cli = '007359034'  and       
	            n_rut_num_usu = '000000001'  and       
	            c_trs_spg     = 'CLT0000120191125125193'  and       
	            n_det_pgo     = CONVERT(int, @i)
	      
	     select @mtomaxdia=v_montmaxdia,@mtomaxtrs=v_montmax 
	     from bd_pri_36.admi.tb_param_spg
		 where c_key like @cspgemppgo+@tipdsp
		 
		 SELECT @totcalc = @totcalc + @detpago
		 SELECT @totcalc
		 IF(@totcalc > @mtomaxdia) 
		 	SELECT 9087
		 	
    IF (@reg = 'F') BREAK                        /* Plog 20201 */
    SELECT @i = @i + 1                           /* Plog 20201 */
  END
  
  SELECT @totcalc

  INSERT INTO bd_pri_36.admi.tb_temp_spg (cspgemppgo, mtomaxdia, mtomaxtrs, detpago, totcalc, descrip) 
  VALUES (@cspgemppgo, @mtomaxdia, @mtomaxtrs, 0, 0, 'despues del ciclo while')

  IF (@mtotrf != @totcalc )                      /* Plog 20201 */
    SELECT 777                                   /* Plog 20201 */

  RETURN 0


go
IF OBJECT_ID('admi.vrf_pgo_spg') IS NOT NULL
    PRINT '<<< CREATED PROC admi.vrf_pgo_spg >>>'
ELSE
    PRINT '<<< FAILED CREATING PROC admi.vrf_pgo_spg >>>'
GO

GRANT Execute ON admi.vrf_pgo_spg TO desa, desa2, desa3, desa4, desa5, desa6, desa7, desa8, desa9, desa10, desa11, desa12
GO